<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class Media extends Enum
{
    const VERIFICATION_MODEL = 'Verification';

    /**
     * @var string[] modelType.
     */
    public static array $modelType = [
        self::VERIFICATION_MODEL,
    ];


    const VERIFICATION_COLLECTION = 'Verification';





    /**
     * @var string[] videoPath.
     */
    public static array $videoPath = [
       'mp4'
    ];

    /**
     * @var string[] filePath.
     */
    public static array $filePath = [
        'png',
        'jpg',
        'jpeg',
        'webp',
        'pdf'
    ];

}
