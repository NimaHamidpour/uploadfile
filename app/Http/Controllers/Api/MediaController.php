<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MediaRequest;
use App\Http\Resources\MediaResource;
use App\Traits\mediaTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MediaController extends Controller
{
    use mediaTrait,MediaRequest;

    /**
     * @param Request $request
     * @return MediaResource
     */
    public function uploadFile(Request $request):MediaResource
    {
        $this->fileValidation($request);
        $model = $this->getModel($request->model_type, $request->model_id);
        $file  = $request->media;
        $collection = $this->getCollection($request->model_type);
        $result = $model->addMedia($file)
                  ->usingFileName($request->model_type .'-' . Str::random(12))
                  ->toMediaCollection($collection);
        return new MediaResource($result);
    }

    /**
     * @param Request $request
     * @return  MediaResource
     */
    public function uploadVideo(Request $request):MediaResource
    {
        $this->imageValidation($request);
        $model = $this->getModel($request->model_type, $request->model_id);
        $file = $request->media;
        $collection = $this->getCollection($request->model_type);
        $result = $model->addMedia($file)
            ->usingFileName(Str::random(10))
            ->toMediaCollection($collection);
        return new MediaResource($result);
    }
}
