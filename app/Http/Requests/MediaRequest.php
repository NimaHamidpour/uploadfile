<?php

namespace App\Http\Requests;

use App\Enum\Media;
use Illuminate\Http\Request;

trait MediaRequest
{

    public function fileValidation(Request  $request)
    {
        $this->validate($request,[
            'model_type' => 'required|in:' . implode(',', Media::$modelType),
            'media'      => 'required|mimes:'. implode(',', Media::$filePath),
            'model_id'   => 'required|integer',
        ]);
    }

    public function videoValidation(Request  $request)
    {
        $this->validate($request,[
            'model_type' => 'required|in:' . implode(',', Media::$modelType),
            'media'      => 'required|mimes:'. implode(',', Media::$videoPath),
            'model_id'   => 'required|integer',
        ]);
    }
}
