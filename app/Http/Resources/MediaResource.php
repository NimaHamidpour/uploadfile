<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;


class MediaResource extends BaseResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'model'      => $this->model,
            'collection' => $this->collection_name,
            'media_url'  => $this->getUrl(),
        ];
    }
}
