<?php
namespace App\Traits;

use App\Enum\Media as EnumMedia;
use App\Models\User;

trait mediaTrait
{
    /**
     * @param $model_type
     * @param $model_id
     * @return false
     */
    public function getModel($model_type, $model_id)
    {
        if ($model_type === EnumMedia::VERIFICATION_MODEL)
           return User::find($model_id);
        return false;
    }

    /**
     * @param $model
     * @return false|string
     */
    public function getCollection($model){
        if ($model === EnumMedia::VERIFICATION_MODEL)
           return  EnumMedia::VERIFICATION_COLLECTION;
        return false;
    }
}
