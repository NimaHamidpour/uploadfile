<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->post('/upload/file', 'Api\MediaController@uploadFile');
$router->post('/upload/video', 'Api\MediaController@uploadVideo');
